package com.tecmanic.storemanager.produk;

import android.Manifest;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.NetworkError;
import com.android.volley.error.NoConnectionError;
import com.android.volley.error.TimeoutError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.toolbox.Volley;
import com.tecmanic.storemanager.Adapter.SelectStockListViewAdapter;
import com.tecmanic.storemanager.AppController;
import com.tecmanic.storemanager.Config.BaseURL;
import com.tecmanic.storemanager.Config.SharedPref;
import com.tecmanic.storemanager.Dashboard.AllProducts;
import com.tecmanic.storemanager.Dashboard.StockUpdate;
import com.tecmanic.storemanager.MainActivity;
import com.tecmanic.storemanager.NetworkConnectivity.NetworkConnection;
import com.tecmanic.storemanager.R;
import com.tecmanic.storemanager.util.AndroidMultiPartEntity;
import com.tecmanic.storemanager.util.CustomVolleyJsonRequest;
import com.tecmanic.storemanager.util.Session_management;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class addProduk extends AppCompatActivity {

    private Session_management sessionManagement;
    private String SelectProduct = "";
    private ArrayList<String> Product_List = new ArrayList<>();
    private String Product_Id;
    private ArrayList<String> Product_LIST_ID = new ArrayList<>();
    private TextView ed_kategori;
    int Selected_Stock_product = 0;
    private RadioGroup radioGroupNb;
    private RadioButton radioButtonNb;
    private Button add_product;
    private EditText ed_nama_produk,ed_deskripsi_produk,ed_mrp,ed_price,ed_unit,ed_unit_value,ed_stock_qty;
    private String prod_status="1";
    private ImageView choose_foto,view_foto;

    //Image request code
    private int PICK_IMAGE_REQUEST = 1;

    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;

    //Bitmap to get image from gallery
    private Bitmap bitmap;

    //Uri to store the image uri
    private Uri filePath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_produk);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle("Tambah Products");
        sessionManagement = new Session_management(getApplicationContext());
        initial();
        requestStoragePermission();

    }

    private void requestStoragePermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
            return;

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            //If the user has denied the permission previously your code will come to this block
            //Here you can explain why you need this permission
            //Explain here why you need this permission
        }
        //And finally ask for the permission
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);
    }


    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        //Checking the request code of our request
        if (requestCode == STORAGE_PERMISSION_CODE) {

            //If permission is granted
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Displaying a toast
                Toast.makeText(this, "Permission granted now you can read the storage", Toast.LENGTH_LONG).show();
            } else {
                //Displaying another toast if permission is not granted
                Toast.makeText(this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    //method to show file chooser
    private void showFileChooser() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
    }

    //handling the image chooser activity result
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            filePath = data.getData();
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), filePath);
                view_foto.setImageBitmap(bitmap);

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //method to get the file path from uri
    public String getPath(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();

        return path;
    }

    private void initial() {
        choose_foto = findViewById(R.id.choose_foto);
        view_foto = findViewById(R.id.view_foto);

        ed_kategori = findViewById(R.id.editText_kategori);
        radioGroupNb = findViewById(R.id.radioGroupNb);
        add_product = findViewById(R.id.btn_add_product);
        ed_nama_produk = findViewById(R.id.nm_produk);
        ed_deskripsi_produk = findViewById(R.id.deskripsi_produk);
        ed_mrp = findViewById(R.id.MRP);
        ed_price = findViewById(R.id.Price);
        ed_unit = findViewById(R.id.Unit);
        ed_unit_value = findViewById(R.id.Unit_Value);
        ed_stock_qty = findViewById(R.id.Stock);

        choose_foto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showFileChooser();
            }
        });

        ed_kategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getKategory();
            }
        });

        add_product.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ed_nama_produk.getText().toString().trim().isEmpty() || ed_deskripsi_produk.getText().toString().trim().isEmpty()|| ed_mrp.getText().toString().trim().isEmpty()|| ed_price.getText().toString().trim().isEmpty()
                        || ed_unit.getText().toString().trim().isEmpty()|| ed_unit_value.getText().toString().trim().isEmpty()|| ed_stock_qty.getText().toString().trim().isEmpty()){
                    Toast.makeText(addProduk.this, "Semua field harus di isi..", Toast.LENGTH_SHORT).show();
                }else {
                    getRadioButton();
                    addProduct();
                    Intent intent = new Intent(addProduk.this, AllProducts.class);
                    startActivity(intent);
                    overridePendingTransition(0, 0);
                    finish();
                }
            }
        });
    }

    private void getRadioButton() {
        int selectedId = radioGroupNb.getCheckedRadioButtonId();

        radioButtonNb = findViewById(selectedId);

        if (radioButtonNb.getText().toString().trim().equals("Deactive"))
            prod_status = "0";
        else
            prod_status = "1";


    }

    private void getKategory(){
        Map<String, String> params = new HashMap<String, String>();

        params.put("user_id", sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
        CustomVolleyJsonRequest jsonObjReq = new CustomVolleyJsonRequest(Request.Method.POST,
                BaseURL.GET_CATEGORI, params, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d("TAG", response.toString());

                try {
                    if (response != null && response.length() > 0) {

                        JSONArray obj = response.getJSONArray("data");
                        Product_List.clear();
                        Product_LIST_ID.clear();
                        for (int i = 0; i < obj.length(); i++) {
                            JSONObject obj1 = obj.getJSONObject(i);
                            for (int z=0;z < obj1.getJSONArray("sub_cat").length();z++){
                                Product_List.add("" + obj1.getJSONArray("sub_cat").getJSONObject(z).getString("title"));
                                Product_LIST_ID.add("" + obj1.getJSONArray("sub_cat").getJSONObject(z).getString("id"));
                            }
                        }
                        SelectProductsDialog();
                    } else {

                        Toast.makeText(addProduk.this, "No Data", Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d("TAG", "Error: " + error.getMessage());
                if (error instanceof TimeoutError || error instanceof NoConnectionError) {
                    Toast.makeText(addProduk.this, getResources().getString(R.string.connection_time_out), Toast.LENGTH_SHORT).show();
                }
            }
        });
        AppController.getInstance().addToRequestQueue(jsonObjReq, "tag");
    }

    private void SelectProductsDialog() {
        final Dialog dialog;
        dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.activity_stock_product_dialog);
        final ListView listView = (ListView) dialog.findViewById(R.id.list_product);
        SelectStockListViewAdapter sec = new SelectStockListViewAdapter(this, Product_List);
        listView.setAdapter(sec);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                SelectProduct = (String) adapterView.getItemAtPosition(position);
                ed_kategori.setText(StringUtils.capitalize(Product_List.get(position).toLowerCase().trim()));
                SelectProduct = ed_kategori.getText().toString();
                Selected_Stock_product = position + 1;
                Product_Id = (Product_LIST_ID.get(position));
//                SharedPref.putString(this, BaseURL.KEY_STOCK_PRODUCTS_ID, Product_Id);
                dialog.dismiss();
            }
        });
        dialog.getWindow().getDecorView().setTop(100);
        dialog.getWindow().getDecorView().setLeft(100);
        dialog.show();

    }

    public byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }


    private void addProduct() {

        if (NetworkConnection.connectionChecking(this)) {
            RequestQueue rq = Volley.newRequestQueue(this);
            AndroidMultiPartEntity postReq = new AndroidMultiPartEntity(Request.Method.POST, BaseURL.ADD_PRODUCT,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse  response) {
                            Log.i("eclipse", "Response=" + response);
//                            try {
//                                JSONObject object = new JSONObject(response);
//                                JSONArray Jarray = object.getJSONArray("product");
//                                for (int i = 0; i < Jarray.length(); i++) {
//                                    JSONObject json_data = Jarray.getJSONObject(i);
//                                    String msg = json_data.getString("msg");
//                                    Toast.makeText(getApplicationContext(), "" + msg, Toast.LENGTH_SHORT).show();
//                                }
//                            } catch (JSONException e) {
//                                e.printStackTrace();
//                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    System.out.println("Error [" + error + "]");
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> params = new HashMap<String, String>();
                    params.put("prod_title", ed_nama_produk.getText().toString().trim());
                    params.put("parent", Product_Id);
                    params.put("prod_status", prod_status);
                    params.put("product_description", ed_deskripsi_produk.getText().toString().trim());
                    params.put("price", ed_price.getText().toString().trim());
                    params.put("mrp", ed_mrp.getText().toString().trim());
                    params.put("qty", ed_unit.getText().toString().trim());
                    params.put("unit", ed_unit.getText().toString().trim());
                    params.put("store_id_login", sessionManagement.getUserDetails().get(BaseURL.KEY_ID));
                    params.put("stk_qty", ed_stock_qty.getText().toString().trim());

                    return params;
                }
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    long imagename = System.currentTimeMillis();
                    params.put("prod_img", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                    return params;
                }

            };
            rq.add(postReq);
        } else {
            Intent intent = new Intent(this, NetworkError.class);
            startActivity(intent);
        }
    }

}
